import {
   addNewCodeService,
   getAllCodesService,
   deleteCodeService,
   convertCodeService,
} from "../services/allcodeService";

export const addNewCode = async (req, res) => {
   try {
      const saveCode = await addNewCodeService(req.body);
      res.status(200).json(saveCode);
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
      });
   }
};

export const getAllCodes = async (req, res) => {
   try {
      const codes = await getAllCodesService(req.query.type);

      res.status(200).json(codes);
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
      });
   }
};

export const convertCode = async (req, res) => {
   try {
      const response = await convertCodeService(req.query.key);
      res.status(200).json(response);
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
      });
   }
};

export const deleteCode = async (req, res) => {
   try {
      await deleteCodeService(req.query.id);
      res.status(200).json({
         errCode: 0,
         errMessage: "OK",
      });
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
      });
   }
};
