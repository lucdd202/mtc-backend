import { loginService, registerService } from "../services/authenService";

export const login = async (req, res) => {
   try {
      const data = await loginService(req.body);
      if (data) {
         res.status(200).json(data);
      } else {
         res.status(500).json({
            errCode: 1,
            errMessage: "Login failed",
         });
      }
   } catch (e) {
      res.status(500).json({
         errCode: 2,
         errMessage: "Error from server !",
      });
   }
};

export const register = async (req, res) => {
   try {
      const user = await registerService(req.body);
      res.status(200).json(user);
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
         e,
      });
   }
};
