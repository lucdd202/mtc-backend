import {
   deleteHistoryService,
   getAllHistoriesService,
   getHistoryByIdService,
   switchNotificationService,
} from "../services/historyService";

export const getAllHistories = async (req, res) => {
   try {
      const histories = await getAllHistoriesService(req.body);
      res.status(200).json(histories);
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
      });
   }
};

export const getHistoryById = async (req, res) => {
   try {
      const history = await getHistoryByIdService(req.query);
      res.status(200).json(history);
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
      });
   }
};

export const deleteHistory = async (req, res) => {
   try {
      await deleteHistoryService(req.query.id);
      res.status(200).json({
         errCode: 0,
         errMessage: "OK",
      });
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
      });
   }
};

export const switchNotification = async (req, res) => {
   try {
      const result = await switchNotificationService(req.query.id);
      res.status(200).json(result);
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
      });
   }
};
