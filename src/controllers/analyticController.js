import { getAnalyticDataService } from "../services/analyticService";

export const getAnalyticData = async (req, res) => {
   try {
      const response = await getAnalyticDataService(req.query);
      res.status(200).json(response);
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
      });
   }
};
