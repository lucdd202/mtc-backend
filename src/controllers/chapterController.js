import {
   addNewChapterService,
   countWeeklyChaptersService,
   deleteChapterService,
   getAllChaptersService,
   getChapterByChapterNumberService,
   getChapterByIdService,
   getLatestChaptersService,
   updateChapterService,
} from "../services/chapterService";

export const addNewChapter = async (req, res) => {
   try {
      const saveChapter = await addNewChapterService(req.body);

      res.status(200).json(saveChapter);
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
      });
   }
};

export const getAllChapters = async (req, res) => {
   try {
      const chapters = await getAllChaptersService(req.query.bookId);

      res.status(200).json(chapters);
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
      });
   }
};

export const getChapterById = async (req, res) => {
   try {
      const chapter = await getChapterByIdService(req.query.id);
      res.status(200).json(chapter);
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
      });
   }
};

export const getChapterByNumber = async (req, res) => {
   try {
      const chapter = await getChapterByChapterNumberService(req.body);
      res.status(200).json(chapter);
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
      });
   }
};

export const updateChapter = async (req, res) => {
   try {
      const chapter = await updateChapterService(req.body);
      res.status(200).json(chapter);
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
      });
   }
};

export const deleteChapter = async (req, res) => {
   try {
      await deleteChapterService(req.query.id);
      res.status(200).json({
         errCode: 0,
         errMessage: "OK",
      });
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
      });
   }
};

export const getLatestChapters = async (req, res) => {
   try {
      const latestChapters = await getLatestChaptersService();
      res.status(200).json(latestChapters);
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
      });
   }
};

export const countWeeklyChapters = async (req, res) => {
   try {
      const chapters = await countWeeklyChaptersService(req.query.bookId);
      res.status(200).json(chapters);
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
      });
   }
};
