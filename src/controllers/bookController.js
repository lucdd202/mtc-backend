import {
   addNewBookService,
   deleteBookService,
   getAllBooksService,
   getBookByIdService,
   updateBookService,
   getConvertedBooksService,
   getNewestBooksService,
   getCompletedBooksService,
   getHighestRatedBooksService,
   searchBooksService,
   getBooksByRankService,
} from "../services/bookService";

export const addNewBook = async (req, res) => {
   try {
      const saveBook = await addNewBookService(req.body);
      res.status(200).json(saveBook);
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
      });
   }
};

export const getAllBooks = async (req, res) => {
   try {
      const books = await getAllBooksService();

      res.status(200).json(books);
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
      });
   }
};

export const getCompletedBooks = async (req, res) => {
   try {
      const books = await getCompletedBooksService();

      res.status(200).json(books);
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
      });
   }
};

export const getHighestRatedBooks = async (req, res) => {
   try {
      const books = await getHighestRatedBooksService();

      res.status(200).json(books);
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
      });
   }
};

//Cho converter
export const getConvertedBooks = async (req, res) => {
   try {
      const books = await getConvertedBooksService(req.query.converterId);
      res.status(200).json(books);
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
      });
   }
};

export const getBookById = async (req, res) => {
   try {
      const book = await getBookByIdService(req.query);

      res.status(200).json(book);
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
      });
   }
};

export const updateBook = async (req, res) => {
   try {
      const book = await updateBookService(req.body);

      res.status(200).json(book);
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
      });
   }
};

export const deleteBook = async (req, res) => {
   try {
      const book = await deleteBookService(req.query.id);
      res.status(200).json({
         errCode: 0,
         errMessage: "OK",
      });
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
      });
   }
};

export const getNewestBooks = async (req, res) => {
   try {
      const books = await getNewestBooksService();
      res.status(200).json(books);
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
      });
   }
};

export const searchBooks = async (req, res) => {
   try {
      const books = await searchBooksService(req.body);
      res.status(200).json(books);
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
      });
   }
};

export const getBookByRank = async (req, res) => {
   try {
      const books = await getBooksByRankService(req.body);
      res.status(200).json(books);
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
      });
   }
};
