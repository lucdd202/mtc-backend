import {
   addNewBannerService,
   deleteBannerService,
   getAllBannersService,
   getBannerByIdService,
   updateBannerService,
   getRandomBannerService,
} from "../services/bannerService";

export const addNewBanner = async (req, res) => {
   try {
      const saveBanner = await addNewBannerService(req.body);
      res.status(200).json(saveBanner);
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
      });
   }
};

export const getAllBanners = async (req, res) => {
   try {
      const banners = await getAllBannersService();

      res.status(200).json(banners);
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
      });
   }
};

export const getRandomBanner = async (req, res) => {
   try {
      const banners = await getRandomBannerService(req.query.number);
      res.status(200).json(banners);
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
      });
   }
};

export const getBannerById = async (req, res) => {
   try {
      const banner = await getBannerByIdService(req.query.id);
      res.status(200).json(banner);
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
      });
   }
};

export const updateBanner = async (req, res) => {
   try {
      const banner = await updateBannerService(req.body);
      res.status(200).json(banner);
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
      });
   }
};

export const deleteBanner = async (req, res) => {
   try {
      const banner = await deleteBannerService(req.query.id);
      res.status(200).json({
         errCode: 0,
         errMessage: "OK",
      });
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
      });
   }
};
