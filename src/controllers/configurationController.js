import {
   getConfigurationService,
   updateConfigurationService,
} from "../services/configurationService";

export const updateConfiguration = async (req, res) => {
   try {
      const result = await updateConfigurationService(req.body);
      res.status(200).json(result);
   } catch (e) {
      console.log(e);
   }
};

export const getConfiguration = async (req, res) => {
   try {
      const result = await getConfigurationService(req.query.userId);
      res.status(200).json(result);
   } catch (e) {
      console.log(e);
   }
};
