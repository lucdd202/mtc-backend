import {
   addNewCommentService,
   deleteCommentService,
   getAllCommentsService,
   getCommentRepliesService,
   replyCommentService,
} from "../services/commentService";

export const addNewComment = async (req, res) => {
   try {
      const saveComment = await addNewCommentService(req.body);
      res.status(200).json(saveComment);
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
      });
   }
};

export const replyComment = async (req, res) => {
   try {
      const saveComment = await replyCommentService(req.body);
      res.status(200).json(saveComment);
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
      });
   }
};

export const getAllComments = async (req, res) => {
   try {
      const comments = await getAllCommentsService(req.query.bookId);

      res.status(200).json(comments);
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
      });
   }
};

export const getCommentReplies = async (req, res) => {
   try {
      const comment = await getCommentRepliesService(req.query.replyToId);
      res.status(200).json(comment);
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
      });
   }
};

export const deleteComment = async (req, res) => {
   try {
      const comment = await deleteCommentService(req.query.id);
      res.status(200).json({
         errCode: 0,
         errMessage: "OK",
      });
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
      });
   }
};
