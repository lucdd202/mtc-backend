import {
   checkNotificationService,
   deleteNotificationService,
   getAllNotificationsService,
   rejectNotificationService,
} from "../services/notificationService";

export const getAllNotifications = async (req, res) => {
   try {
      const notifications = await getAllNotificationsService(req.query.userId);
      res.status(200).json(notifications);
   } catch (e) {
      console.log(e);
      res.status(500).json({
         errMessage: "Error from server!",
      });
   }
};

export const rejectNotification = async (req, res) => {
   try {
      const notification = await rejectNotificationService(req.query.id);
      res.status(200).json(notification);
   } catch (e) {
      console.log(e);
      res.status(500).json({
         errMessage: "Error from server!",
      });
   }
};

export const deleteNotification = async (req, res) => {
   try {
      const notification = await deleteNotificationService(req.query.id);
      res.status(200).json(notification);
   } catch (e) {
      console.log(e);
      res.status(500).json({
         errMessage: "Error from server!",
      });
   }
};

export const checkNotification = async (req, res) => {
   try {
      const notification = await checkNotificationService(req.body);
      res.status(200).json(notification);
   } catch (e) {
      console.log(e);
      res.status(500).json({
         errMessage: "Error from server!",
      });
   }
};
