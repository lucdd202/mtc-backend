import {
   addNewUserService,
   deleteUserService,
   getAllUsersService,
   getUserByIdService,
   updateUserService,
   giveFlowerService,
   changePasswordService,
} from "../services/userService";

export const addNewUser = async (req, res) => {
   try {
      const user = await addNewUserService(req.body);
      res.status(200).json(user);
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
      });
   }
};

export const getAllUsers = async (req, res) => {
   try {
      const users = await getAllUsersService();
      res.status(200).json(users);
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
      });
   }
};

export const getUserById = async (req, res) => {
   try {
      const user = await getUserByIdService(req.query.id);
      res.status(200).json(user);
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
      });
   }
};

export const updateUser = async (req, res) => {
   try {
      const user = await updateUserService(req.body);
      res.status(200).json(user);
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
      });
   }
};

export const deleteUser = async (req, res) => {
   try {
      await deleteUserService(req.query.id);
      res.status(200).json({
         errCode: 0,
         errMessage: "OK",
      });
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
      });
   }
};

export const giveFlower = async (req, res) => {
   try {
      const give = await giveFlowerService(req.body);
      res.status(200).json(give);
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
      });
   }
};

export const changePassword = async (req, res) => {
   try {
      const response = await changePasswordService(req.body);
      res.status(200).json(response);
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
      });
   }
};
