import {
   addRemoveBookmarkService,
   getAllBookmarksService,
   isBookmaredService,
} from "../services/bookmarkService";

export const addRemoveBookmark = async (req, res) => {
   try {
      const saveBookmark = await addRemoveBookmarkService(req.body);
      res.status(200).json(saveBookmark);
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
      });
   }
};

export const isBookMarked = async (req, res) => {
   try {
      const check = await isBookmaredService(req.body);
      res.status(200).json(check);
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
      });
   }
};

export const getAllBookmarks = async (req, res) => {
   try {
      const bookmarks = await getAllBookmarksService(req.query.userId);
      res.status(200).json(bookmarks);
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
      });
   }
};
