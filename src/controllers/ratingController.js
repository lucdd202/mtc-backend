import {
   createNewRatingService,
   deleteRatingService,
   getAllRatingService,
   getNewestRatingsService,
   getRatingRepliesService,
   replyRatingService,
} from "../services/ratingService";

export const getAllRatings = async (req, res) => {
   try {
      const ratings = await getAllRatingService(req.query.bookId);
      res.status(200).json(ratings);
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
      });
   }
};

export const createNewRating = async (req, res) => {
   try {
      const rating = await createNewRatingService(req.body);
      res.status(200).json(rating);
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
      });
   }
};

export const deleteRating = async (req, res) => {
   try {
      const rating = await deleteRatingService(req.query.id);
      res.status(200).json(rating);
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
      });
   }
};

export const replyRating = async (req, res) => {
   try {
      const saveReply = await replyRatingService(req.body);
      res.status(200).json(saveReply);
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
      });
   }
};

export const getNewestRatings = async (req, res) => {
   try {
      const ratings = await getNewestRatingsService(req.query.limit);
      res.status(200).json(ratings);
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
      });
   }
};

export const getRatingReplies = async (req, res) => {
   try {
      const ratings = await getRatingRepliesService(req.query.replyToId);
      res.status(200).json(ratings);
   } catch (e) {
      res.status(500).json({
         errCode: 0,
         errMessage: "Error from server !",
      });
   }
};
