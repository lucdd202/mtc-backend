"use strict";
/** @type {import('sequelize-cli').Migration} */
module.exports = {
   async up(queryInterface, Sequelize) {
      await queryInterface.createTable("Ratings", {
         id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER,
         },
         bookId: {
            type: Sequelize.INTEGER,
         },
         replyToId: {
            type: Sequelize.INTEGER,
         },
         userId: {
            type: Sequelize.INTEGER,
         },
         characterPoint: {
            type: Sequelize.FLOAT,
         },
         worldPoint: {
            type: Sequelize.FLOAT,
         },
         storyPoint: {
            type: Sequelize.FLOAT,
         },
         point: {
            type: Sequelize.FLOAT,
         },
         content: {
            type: Sequelize.TEXT,
         },

         createdAt: {
            allowNull: false,
            type: Sequelize.DATE,
         },
         updatedAt: {
            allowNull: false,
            type: Sequelize.DATE,
         },
      });
   },
   async down(queryInterface, Sequelize) {
      await queryInterface.dropTable("Ratings");
   },
};
