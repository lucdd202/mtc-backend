"use strict";
/** @type {import('sequelize-cli').Migration} */
module.exports = {
   async up(queryInterface, Sequelize) {
      await queryInterface.createTable("BookInfos", {
         id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER,
         },
         bookId: {
            type: Sequelize.INTEGER,
         },
         worldId: {
            type: Sequelize.STRING,
         },
         featureId: {
            type: Sequelize.STRING,
         },
         genreId: {
            type: Sequelize.STRING,
         },
         povId: {
            type: Sequelize.STRING,
         },
         statusId: {
            type: Sequelize.STRING,
         },
         characterId: {
            type: Sequelize.STRING,
         },
         authorId: {
            type: Sequelize.STRING,
         },
         createdAt: {
            allowNull: false,
            type: Sequelize.DATE,
         },
         updatedAt: {
            allowNull: false,
            type: Sequelize.DATE,
         },
      });
   },
   async down(queryInterface, Sequelize) {
      await queryInterface.dropTable("BookInfos");
   },
};
