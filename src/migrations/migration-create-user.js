"use strict";
/** @type {import('sequelize-cli').Migration} */
module.exports = {
   async up(queryInterface, Sequelize) {
      await queryInterface.createTable("Users", {
         id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER,
         },
         name: {
            type: Sequelize.STRING,
         },
         password: {
            type: Sequelize.STRING,
         },
         email: {
            type: Sequelize.STRING,
         },
         avatar: {
            type: Sequelize.BLOB,
         },
         roleId: {
            type: Sequelize.STRING,
         },
         quote: {
            type: Sequelize.STRING,
         },
         flower: {
            type: Sequelize.INTEGER,
         },
         flowerGive: {
            type: Sequelize.INTEGER,
         },
         chapterRead: {
            type: Sequelize.INTEGER,
         },
         createdAt: {
            allowNull: false,
            type: Sequelize.DATE,
         },
         updatedAt: {
            allowNull: false,
            type: Sequelize.DATE,
         },
      });
   },
   async down(queryInterface, Sequelize) {
      await queryInterface.dropTable("Users");
   },
};
