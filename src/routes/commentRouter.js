import express from "express";
import { commentController } from "../controllers";
const commentRouter = express.Router();

commentRouter.get("/get-all-comments", commentController.getAllComments);
commentRouter.get("/get-all-replies", commentController.getCommentReplies);
commentRouter.post("/add-new-comment", commentController.addNewComment);
commentRouter.post("/reply-comment", commentController.replyComment);
commentRouter.delete("/delete-comment", commentController.deleteComment);

export default commentRouter;
