import express from "express";
import { analyticController } from "../controllers";
const analyticRouter = express.Router();

analyticRouter.get("/get-analytic-data", analyticController.getAnalyticData);

export default analyticRouter;
