import express from "express";
import { authenController } from "../controllers";

const authenRouter = express.Router();

authenRouter.post("/login", authenController.login);
authenRouter.post("/register", authenController.register);

export default authenRouter;
