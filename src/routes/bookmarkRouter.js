import express from "express";
import { bookmarkController } from "../controllers";
const bookmarkRouter = express.Router();

bookmarkRouter.get("/get-all-bookmarks", bookmarkController.getAllBookmarks);
bookmarkRouter.post("/is-bookmarked", bookmarkController.isBookMarked);
bookmarkRouter.post(
   "/add-remove-bookmark",
   bookmarkController.addRemoveBookmark
);

export default bookmarkRouter;
