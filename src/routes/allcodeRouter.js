import express from "express";
import { allcodeController } from "../controllers";
const allcodeRouter = express.Router();

allcodeRouter.get("/get-all-codes", allcodeController.getAllCodes);
allcodeRouter.post("/add-new-code", allcodeController.addNewCode);
allcodeRouter.get("/convert-code", allcodeController.convertCode);
allcodeRouter.delete("/delete-code", allcodeController.deleteCode);

export default allcodeRouter;
