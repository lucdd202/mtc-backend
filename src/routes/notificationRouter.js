import express from "express";
import { notificationController } from "../controllers";
const notificationRouter = express.Router();

notificationRouter.get(
   "/get-all-notifications",
   notificationController.getAllNotifications
);
notificationRouter.delete(
   "/delete-notification",
   notificationController.deleteNotification
);
notificationRouter.post(
   "/check-notification",
   notificationController.checkNotification
);
notificationRouter.put(
   "/reject-notification",
   notificationController.rejectNotification
);

export default notificationRouter;
