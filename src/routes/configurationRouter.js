import express from "express";
import { configurationController } from "../controllers";
const configurationRouter = express.Router();

configurationRouter.post(
   "/update-configuration",
   configurationController.updateConfiguration
);

configurationRouter.get(
   "/get-configuration",
   configurationController.getConfiguration
);

export default configurationRouter;
