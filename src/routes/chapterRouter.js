import express from "express";
import { chapterController } from "../controllers";
const chapterRouter = express.Router();

chapterRouter.get("/get-all-chapters", chapterController.getAllChapters);
chapterRouter.get("/get-chapter-by-id", chapterController.getChapterById);
chapterRouter.get(
   "/count-weekly-chapters",
   chapterController.countWeeklyChapters
);
chapterRouter.post(
   "/get-chapter-by-number",
   chapterController.getChapterByNumber
);
chapterRouter.get("/get-latest-chapters", chapterController.getLatestChapters);
chapterRouter.post("/add-new-chapter", chapterController.addNewChapter);
chapterRouter.put("/update-chapter", chapterController.updateChapter);
chapterRouter.delete("/delete-chapter", chapterController.deleteChapter);

export default chapterRouter;
