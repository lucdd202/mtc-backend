import express from "express";
import { userController } from "../controllers";
const userRouter = express.Router();

userRouter.get("/get-all-users", userController.getAllUsers);
userRouter.get("/get-user-by-id", userController.getUserById);
userRouter.post("/add-new-user", userController.addNewUser);
userRouter.post("/give-flower", userController.giveFlower);
userRouter.put("/update-user", userController.updateUser);
userRouter.put("/change-password", userController.changePassword);
userRouter.delete("/delete-user", userController.deleteUser);

export default userRouter;
