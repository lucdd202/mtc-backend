import express from "express";
import { bookController } from "../controllers";
const bookRouter = express.Router();

bookRouter.get("/get-all-books", bookController.getAllBooks);
bookRouter.get("/get-completed-books", bookController.getCompletedBooks);
bookRouter.get("/get-highest-rated-books", bookController.getHighestRatedBooks);
bookRouter.get("/get-newest-books", bookController.getNewestBooks);
bookRouter.get("/get-book-by-id", bookController.getBookById);
bookRouter.post("/add-new-book", bookController.addNewBook);
bookRouter.post("/search-books", bookController.searchBooks);
bookRouter.post("/get-books-by-rank", bookController.getBookByRank);
bookRouter.put("/update-book", bookController.updateBook);
bookRouter.delete("/delete-book", bookController.deleteBook);
bookRouter.get("/get-converted-books", bookController.getConvertedBooks);

export default bookRouter;
