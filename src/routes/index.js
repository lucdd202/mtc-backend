import express from "express";
import authenRouter from "./authenRouter";
import userRouter from "./userRouter";
import bookRouter from "./bookRouter";
import allcodeRouter from "./allcodeRouter";
import chapterRouter from "./chapterRouter";
import historyRouter from "./historyRouter";
import bookmarkRouter from "./bookmarkRouter";
import commentRouter from "./commentRouter";
import bannerRouter from "./bannerRouter";
import analyticRouter from "./analyticRouter";
import ratingRouter from "./ratingRouter";
import configurationRouter from "./configurationRouter";
import notificationRouter from "./notificationRouter";

let router = express.Router();

const initWebRoutes = (app) => {
   app.use("/api/authen", authenRouter);
   app.use("/api/user", userRouter);
   app.use("/api/book", bookRouter);
   app.use("/api/allcode", allcodeRouter);
   app.use("/api/chapter", chapterRouter);
   app.use("/api/history", historyRouter);
   app.use("/api/bookmark", bookmarkRouter);
   app.use("/api/comment", commentRouter);
   app.use("/api/banner", bannerRouter);
   app.use("/api/analytic", analyticRouter);
   app.use("/api/rating", ratingRouter);
   app.use("/api/configuration", configurationRouter);
   app.use("/api/notification", notificationRouter);
   return app.use("/", router);
};

export default initWebRoutes;
