import express from "express";
import { bannerController } from "../controllers";
const bannerRouter = express.Router();

bannerRouter.get("/get-all-banners", bannerController.getAllBanners);
bannerRouter.get("/get-banner-by-id", bannerController.getBannerById);
bannerRouter.post("/add-new-banner", bannerController.addNewBanner);
bannerRouter.put("/update-banner", bannerController.updateBanner);
bannerRouter.delete("/delete-banner", bannerController.deleteBanner);
bannerRouter.get("/get-random-banner", bannerController.getRandomBanner);

export default bannerRouter;
