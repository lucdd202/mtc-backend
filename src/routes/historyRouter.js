import express from "express";
import { historyController } from "../controllers";
const historyRouter = express.Router();

historyRouter.post("/get-all-histories", historyController.getAllHistories);
historyRouter.get("/get-history-by-id", historyController.getHistoryById);
historyRouter.delete("/delete-history", historyController.deleteHistory);
historyRouter.put("/switch-notification", historyController.switchNotification);

export default historyRouter;
