import express from "express";
import { ratingController } from "../controllers";
const ratingRouter = express.Router();

ratingRouter.get("/get-all-ratings", ratingController.getAllRatings);
ratingRouter.get("/get-all-replies", ratingController.getRatingReplies);
ratingRouter.get("/get-newest-ratings", ratingController.getNewestRatings);
ratingRouter.post("/create-new-rating", ratingController.createNewRating);
ratingRouter.post("/reply-rating", ratingController.replyRating);
ratingRouter.delete("/delete-rating", ratingController.deleteRating);

export default ratingRouter;
