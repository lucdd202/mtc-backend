import express from "express";
import cors from "cors";
import connectDB from "./config/connectDB.js";
import bodyParser from "body-parser";
import dotenv from "dotenv";
import morgan from "morgan";
import initWebRoutes from "./routes";
import viewEngine from "./config/viewEngine.js";
import { resetAnalyticService } from "./services/analyticService.js";
import { resetFlowerService } from "./services/userService.js";
const schedule = require("node-schedule");

dotenv.config();

const app = express();
const port = process.env.PORT || 8080;

const corsOptions = {
   origin: "http://localhost:3000",
   credentials: true, //access-control-allow-credentials:true
   optionSuccessStatus: 200,
};
app.use(cors(corsOptions));
app.use(bodyParser.json({ limit: "50mb" }));
app.use(bodyParser.urlencoded({ limit: "50mb", extended: true }));
app.use(morgan("common"));

initWebRoutes(app);
viewEngine(app);

connectDB();

app.listen(port, () => {
   console.log(`Server is running on Port ${port} !`);
});

schedule.scheduleJob("0 0 * * 1", async () => {
   try {
      await resetAnalyticService("WEEK");
   } catch (e) {
      console.error("Lỗi trong quá trình đặt lại theo tuần:", e);
   }
});

schedule.scheduleJob("0 0 1 * *", async () => {
   try {
      await resetAnalyticService("MONTH");
   } catch (e) {
      console.error("Lỗi trong quá trình đặt lại theo tháng:", e);
   }
});

schedule.scheduleJob("0 0 * * *", async () => {
   try {
      await resetFlowerService();
   } catch (e) {
      console.error("Lỗi trong quá trình đặt lại hoa:", e);
   }
});
