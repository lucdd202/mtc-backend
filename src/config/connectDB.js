const { Sequelize } = require("sequelize");
import dotenv from "dotenv";

dotenv.config();

const sequelize = new Sequelize(
   "mtc",
   "postgres",
   process.env.POSTGRES_PASSWORD,
   {
      host: "localhost",
      dialect: "postgres",
      logging: false,
   }
);

let connectDB = async () => {
   try {
      await sequelize.authenticate();
      console.log("Thiết lập kết nối DB thành công");
   } catch (error) {
      console.error("Không thể kết nối đến DB", error);
   }
};

module.exports = connectDB;
