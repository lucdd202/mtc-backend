"use strict";
const { Model, INTEGER } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
   class User extends Model {
      /**
       * Helper method for defining associations.
       * This method is not a part of Sequelize lifecycle.
       * The `models/index` file will call this method automatically.
       */
      static associate(models) {
         // define association here
         User.belongsTo(models.Allcode, {
            targetKey: "key",
            foreignKey: "roleId",
            as: "Role",
         });
         User.hasMany(models.Book, {
            foreignKey: "converterId",
            as: "ConvertedBook",
         });
         User.hasMany(models.History, { foreignKey: "userId" });
         User.hasMany(models.Bookmark, { foreignKey: "userId" });
         User.hasMany(models.Comment, { foreignKey: "userId" });
         User.hasOne(models.Rating, { foreignKey: "userId" });
         User.hasMany(models.Notification, { foreignKey: "fromId" });
      }
   }
   User.init(
      {
         name: DataTypes.STRING,
         email: DataTypes.STRING,
         password: DataTypes.STRING,
         roleId: DataTypes.STRING,
         avatar: DataTypes.BLOB,
         flower: DataTypes.INTEGER,
         flowerGive: DataTypes.INTEGER,
         chapterRead: DataTypes.INTEGER,
         quote: DataTypes.STRING,
      },
      {
         sequelize,
         modelName: "User",
      }
   );
   return User;
};
