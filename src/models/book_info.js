"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
   class BookInfo extends Model {
      /**
       * Helper method for defining associations.
       * This method is not a part of Sequelize lifecycle.
       * The `models/index` file will call this method automatically.
       */
      static associate(models) {
         // define association here
         BookInfo.belongsTo(models.Book, {
            foreignKey: "bookId",
            targetKey: "id",
         });
         BookInfo.belongsTo(models.Allcode, {
            foreignKey: "authorId",
            targetKey: "key",
            as: "Author",
         });
         BookInfo.belongsTo(models.Allcode, {
            foreignKey: "genreId",
            targetKey: "key",
            as: "Genre",
         });
         BookInfo.belongsTo(models.Allcode, {
            foreignKey: "characterId",
            targetKey: "key",
            as: "Character",
         });
         BookInfo.belongsTo(models.Allcode, {
            foreignKey: "povId",
            targetKey: "key",
            as: "POV",
         });
         BookInfo.belongsTo(models.Allcode, {
            foreignKey: "worldId",
            targetKey: "key",
            as: "world",
         });
         BookInfo.belongsTo(models.Allcode, {
            foreignKey: "featureId",
            targetKey: "key",
            as: "Feature",
         });
         BookInfo.belongsTo(models.Allcode, {
            foreignKey: "statusId",
            targetKey: "key",
            as: "Status",
         });
         BookInfo.hasOne(models.Analytic, { foreignKey: "bookId" });
      }
   }
   BookInfo.init(
      {
         bookId: DataTypes.INTEGER,
         characterId: DataTypes.STRING,
         worldId: DataTypes.STRING,
         featureId: DataTypes.STRING,
         povId: DataTypes.STRING,
         statusId: DataTypes.STRING,
         genreId: DataTypes.STRING,
         authorId: DataTypes.STRING,
      },
      {
         sequelize,
         modelName: "BookInfo",
      }
   );
   return BookInfo;
};
