"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
   class Rating extends Model {
      /**
       * Helper method for defining associations.
       * This method is not a part of Sequelize lifecycle.
       * The `models/index` file will call this method automatically.
       */
      static associate(models) {
         // define association here
         Rating.belongsTo(models.User, {
            foreignKey: "userId",
            targetKey: "id",
         });
         Rating.belongsTo(models.Book, {
            foreignKey: "bookId",
            targetKey: "id",
         });
      }
   }
   Rating.init(
      {
         bookId: DataTypes.INTEGER,
         userId: DataTypes.INTEGER,
         characterPoint: DataTypes.FLOAT,
         worldPoint: DataTypes.FLOAT,
         storyPoint: DataTypes.FLOAT,
         point: DataTypes.FLOAT,
         content: DataTypes.TEXT,
         replyToId: DataTypes.INTEGER,
      },
      {
         sequelize,
         modelName: "Rating",
      }
   );
   return Rating;
};
