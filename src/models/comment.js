"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
   class Comment extends Model {
      /**
       * Helper method for defining associations.
       * This method is not a part of Sequelize lifecycle.
       * The `models/index` file will call this method automatically.
       */
      static associate(models) {
         // define association here
         Comment.belongsTo(models.User, {
            foreignKey: "userId",
            targetKey: "id",
         });
         Comment.belongsTo(models.Book, {
            foreignKey: "bookId",
            targetKey: "id",
         });
         Comment.hasMany(Comment, {
            foreignKey: "replyToId",
         });
      }
   }
   Comment.init(
      {
         content: DataTypes.TEXT,
         userId: DataTypes.INTEGER,
         bookId: DataTypes.INTEGER,
         replyToId: DataTypes.INTEGER,
         commentAt: DataTypes.INTEGER,
         isNotification: DataTypes.BOOLEAN,
      },
      {
         sequelize,
         modelName: "Comment",
      }
   );
   return Comment;
};
