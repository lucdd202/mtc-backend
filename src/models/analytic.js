"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
   class Analytic extends Model {
      /**
       * Helper method for defining associations.
       * This method is not a part of Sequelize lifecycle.
       * The `models/index` file will call this method automatically.
       */
      static associate(models) {
         // define association here
         Analytic.belongsTo(models.Book, {
            foreignKey: "bookId",
            targetKey: "id",
         });
         Analytic.belongsTo(models.BookInfo, {
            foreignKey: "bookId",
            targetKey: "bookId",
         });
      }
   }
   Analytic.init(
      {
         bookId: DataTypes.INTEGER,
         key: DataTypes.STRING,
         count: DataTypes.INTEGER,
         type: DataTypes.STRING,
      },
      {
         sequelize,
         modelName: "Analytic",
      }
   );
   return Analytic;
};
