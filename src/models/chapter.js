"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
   class Chapter extends Model {
      /**
       * Helper method for defining associations.
       * This method is not a part of Sequelize lifecycle.
       * The `models/index` file will call this method automatically.
       */
      static associate(models) {
         // define association here
         Chapter.belongsTo(models.Book, {
            foreignKey: "bookId",
            targetKey: "id",
         });
      }
   }
   Chapter.init(
      {
         bookId: DataTypes.INTEGER,
         title: DataTypes.STRING,
         content: DataTypes.TEXT,
         chapterNumber: DataTypes.INTEGER,
      },
      {
         sequelize,
         modelName: "Chapter",
      }
   );
   return Chapter;
};
