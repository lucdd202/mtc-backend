"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
   class Book extends Model {
      /**
       * Helper method for defining associations.
       * This method is not a part of Sequelize lifecycle.
       * The `models/index` file will call this method automatically.
       */
      static associate(models) {
         // define association here
         Book.hasOne(models.BookInfo, { foreignKey: "bookId" });
         Book.belongsTo(models.User, {
            foreignKey: "converterId",
            targetKey: "id",
            as: "Converter",
         });
         Book.hasMany(models.Chapter, {
            foreignKey: "bookId",
         });
         Book.hasMany(models.Bookmark, { foreignKey: "bookId" });
         Book.hasOne(models.History, { foreignKey: "bookId" });
         Book.hasOne(models.Banner, { foreignKey: "bookId" });
         Book.hasMany(models.Report, { foreignKey: "entityId" });
         Book.hasMany(models.Comment, { foreignKey: "bookId" });
         Book.hasMany(models.Rating, { foreignKey: "bookId" });
         Book.hasMany(models.Analytic, {
            foreignKey: "bookId",
         });
      }
   }
   Book.init(
      {
         name: DataTypes.STRING,
         image: DataTypes.BLOB,
         description: DataTypes.TEXT,
         converterId: DataTypes.INTEGER,
      },
      {
         sequelize,
         modelName: "Book",
      }
   );
   return Book;
};
