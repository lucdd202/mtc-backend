"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
   class Allcode extends Model {
      /**
       * Helper method for defining associations.
       * This method is not a part of Sequelize lifecycle.
       * The `models/index` file will call this method automatically.
       */
      static associate(models) {
         // define association here
         Allcode.hasMany(models.User, {
            foreignKey: "roleId",
            as: "Role",
         });
         Allcode.hasOne(models.BookInfo, {
            foreignKey: "authorId",
            as: "Author",
         });
         Allcode.hasOne(models.BookInfo, {
            foreignKey: "genreId",
            as: "Genre",
         });
         Allcode.hasOne(models.BookInfo, {
            foreignKey: "characterId",
            as: "Character",
         });
         Allcode.hasOne(models.BookInfo, {
            foreignKey: "povId",
            as: "POV",
         });
         Allcode.hasOne(models.BookInfo, {
            foreignKey: "worldId",
            as: "world",
         });
         Allcode.hasOne(models.BookInfo, {
            foreignKey: "featureId",
            as: "Feature",
         });
         Allcode.hasOne(models.BookInfo, {
            foreignKey: "statusId",
            as: "Status",
         });
      }
   }
   Allcode.init(
      {
         type: DataTypes.STRING,
         key: DataTypes.STRING,
         value: DataTypes.STRING,
      },
      {
         sequelize,
         modelName: "Allcode",
      }
   );
   return Allcode;
};
