"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
   class History extends Model {
      /**
       * Helper method for defining associations.
       * This method is not a part of Sequelize lifecycle.
       * The `models/index` file will call this method automatically.
       */
      static associate(models) {
         // define association here
         History.belongsTo(models.User, {
            foreignKey: "userId",
            targetKey: "id",
         });
         History.belongsTo(models.Book, {
            foreignKey: "bookId",
            targetKey: "id",
         });
      }
   }
   History.init(
      {
         bookId: DataTypes.INTEGER,
         userId: DataTypes.INTEGER,
         readAt: DataTypes.INTEGER,
         isNotification: DataTypes.BOOLEAN,
      },
      {
         sequelize,
         modelName: "History",
      }
   );
   return History;
};
