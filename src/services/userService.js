import db from "../models";
import bcrypt from "bcrypt";
import { updateAnalyticService } from "./analyticService";

const salt = bcrypt.genSaltSync(10);

export const addNewUserService = async (data) => {
   try {
      let hashPassword = bcrypt.hashSync(data.password, salt);
      await db.User.create({
         name: data.name,
         email: data.email,
         password: hashPassword,
         roleId: data.roleId,
         avatar: data.avatar,
         flower: 2,
         flowerGive: 0,
         chapterRead: 0,
      });
      return { message: "Add new user succeed !" };
   } catch (e) {
      console.log("Add new user failed !");
      console.log(e);
   }
};

export const getAllUsersService = async () => {
   try {
      const users = await db.User.findAll({
         include: [
            {
               model: db.Allcode,
               as: "Role",
               attributes: ["value"],
            },
         ],
         attributes: {
            exclude: ["password"],
         },
         raw: true,
         nest: true,
      });
      //thích thì thêm limit
      console.log("Fetch all users succeed!");
      return users;
   } catch (e) {
      console.log("Fetch all users failed!");
      console.log(e);
   }
};

export const getUserByIdService = async (idInput) => {
   try {
      const user = await db.User.findOne({
         where: { id: idInput },
         attributes: {
            exclude: ["password"],
         },
         raw: true,
      });

      const commentCount = await db.Comment.count({
         where: { userId: idInput },
      });
      const historyCount = await db.History.count({
         where: { userId: idInput },
      });
      const bookmarkCount = await db.Bookmark.count({
         where: { userId: idInput },
      });
      const convertedCount = await db.Book.count({
         where: { converterId: idInput },
      });

      user.commentCount = commentCount;
      user.historyCount = historyCount;
      user.bookmarkCount = bookmarkCount;
      user.convertedCount = convertedCount;

      return user;
   } catch (e) {
      console.log("User not found!");
      console.log(e);
   }
};

export const updateUserService = async (data) => {
   try {
      const user = await db.User.update(
         {
            name: data.name,
            email: data.email,
            roleId: data.roleId,
            quote: data.quote,
            avatar: data.avatar,
         },
         {
            where: { email: data.email },
         }
      );

      if (!user) {
         console.log("Missing parameter");
      }

      return { message: "Update user succeed!" };
   } catch (e) {
      console.log("Update user failed!");
      console.log(e);
   }
};

export const deleteUserService = async (idInput) => {
   try {
      const user = await db.User.destroy({
         where: { id: idInput },
      });

      return { message: "User deleted!" };
   } catch (e) {
      console.log("Delete User failed!", e);
   }
};

//change password
export const changePasswordService = async (data) => {
   try {
      const user = await db.User.findOne({
         where: { id: data.id },
      });
      if (user) {
         let check = bcrypt.compareSync(data.currentPassword, user.password);
         if (check) {
            let hashPassword = bcrypt.hashSync(data.newPassword, salt);
            user.password = hashPassword;
            await user.save();
            return { errCode: 0, errMessage: "Đổi mật khẩu thành công!" };
         } else {
            return { errCode: 1, errMessage: "Sai mật khẩu hiện tại!" };
         }
      }
      return { errCode: 2, errMessage: "Người dùng không tồn tại!" };
   } catch (e) {
      console.log("Change password failed!", e);
   }
};

export const giveFlowerService = async (data) => {
   try {
      const user = await db.User.findOne({
         where: { id: data.userId },
      });
      if (user) {
         if (user.flower > 0) {
            user.flower -= 1;
            user.flowerGive += 1;
            await user.save();
            updateAnalyticService(data.bookId, "TOTAL", "FLOWER");
            updateAnalyticService(data.bookId, "WEEK", "FLOWER");
            updateAnalyticService(data.bookId, "MONTH", "FLOWER");
            return { code: 0, message: "Tặng hoa thành công" };
         } else {
            return { code: 1, message: "Bạn đã hết hoa vào hôm nay" };
         }
      }
   } catch (e) {
      console.log(e);
   }
};

export const resetFlowerService = async () => {
   try {
      const users = await db.User.findAll();
      if (users) {
         const resetPromises = users.map(async (data) => {
            data.flower = 2;
            await data.save();
         });

         await Promise.all(resetPromises);
      }
   } catch (e) {
      console.log(e);
   }
};
