import db from "../models";

export const addRemoveBookmarkService = async (data) => {
   try {
      const isBookmarked = await db.Bookmark.findOne({
         where: {
            userId: data.userId,
            bookId: data.bookId,
         },
      });
      if (isBookmarked) {
         await db.Bookmark.destroy({
            where: {
               userId: data.userId,
               bookId: data.bookId,
            },
         });
         return { message: "Deleted Bookmark succeed" };
      } else {
         await db.Bookmark.create({
            userId: data.userId,
            bookId: data.bookId,
         });
         return { message: "Add Bookmark succeed" };
      }
   } catch (e) {
      console.log("Add/Remove bookmark failed !", e);
   }
};

export const isBookmaredService = async (data) => {
   try {
      const isBookmarked = await db.Bookmark.findOne({
         where: {
            userId: data.userId,
            bookId: data.bookId,
         },
      });
      return isBookmarked ? true : false;
   } catch (e) {
      console.log("Get Bookmark status failed !", e);
   }
};

export const getAllBookmarksService = async (userId) => {
   try {
      const bookmarks = await db.Bookmark.findAll({
         where: {
            userId: userId,
         },
         include: [
            {
               model: db.Book,
            },
         ],
         nest: true,
         raw: true,
         order: [["createdAt", "DESC"]],
      });

      return bookmarks;
   } catch (e) {
      console.log("Fetch all bookmarks failed!", e);
   }
};
