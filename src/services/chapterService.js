import db from "../models";
import { updateAnalyticService } from "./analyticService";
import { updateCreateHistoryService } from "./historyService";
import { createNotificationService } from "./notificationService";
const { Sequelize, Op } = require("sequelize");

export const addNewChapterService = async (data) => {
   try {
      const chapter = await db.Chapter.create({
         title: data.title,
         content: data.content,
         bookId: +data.bookId,
         chapterNumber: data.chapterNumber,
      });

      const histories = await db.History.findAll({
         where: { isNotification: true, bookId: +data.bookId },
         include: {
            model: db.Book,
            attributes: ["name"],
         },
         nest: true,
      });

      await Promise.all(
         histories.forEach(async (history) => {
            if (history.userId !== history.Book.converterId) {
               await createNotificationService({
                  userId: history.userId,
                  entityId: +data.bookId,
                  content: `${history.Book.name} đã thêm Chương ${data.chapterNumber}: ${data.title}`,
                  fromId: history.Book.converterId,
                  type: "CHAPTER",
               });
            }
         })
      );

      return { message: "Add new chapter succeed !" };
   } catch (e) {
      console.log("Add new chapter failed !", e);
   }
};

export const getAllChaptersService = async (bookId) => {
   try {
      const chapters = await db.Chapter.findAll({
         where: { bookId: bookId },
         raw: true,
      });

      console.log("Fetch all chapters succeed!");
      return chapters;
   } catch (e) {
      console.log("Fetch all chapters failed!", e);
   }
};

export const getChapterByIdService = async (idInput) => {
   try {
      const chapter = await db.Chapter.findOne({
         where: { id: idInput },
         raw: true,
      });
      console.log("Chapter found!");
      return chapter;
   } catch (e) {
      console.log("Chapter not found!", e);
   }
};

export const getChapterByChapterNumberService = async (data) => {
   try {
      console.log("chapter data", data);
      const history = await updateCreateHistoryService(data);

      const user = await db.User.findOne({
         where: { id: data.userId },
      });

      if (user) {
         user.chapterRead += 1;
         await user.save();
      }

      const chapter = await db.Chapter.findOne({
         where: {
            bookId: data.bookId,
            chapterNumber: data.chapterNumber ?? history.readAt,
         },
         raw: true,
      });

      updateAnalyticService(data.bookId, "TOTAL", "VIEW");
      updateAnalyticService(data.bookId, "WEEK", "VIEW");
      updateAnalyticService(data.bookId, "MONTH", "VIEW");

      return chapter;
   } catch (e) {
      console.log("Chapter not found!", e);
   }
};

export const updateChapterService = async (data) => {
   try {
      await db.Chapter.update(
         {
            title: data.title,
            content: data.content,
            bookId: +data.bookId,
            chapterNumber: data.chapterNumber,
         },
         {
            where: { id: data.id },
         }
      );
      return { message: "Update Chapter succeed" };
   } catch (e) {
      console.log("Update chapter failed!", e);
   }
};

export const deleteChapterService = async (idInput) => {
   try {
      await db.Chapter.destroy({
         where: { id: idInput },
      });
      return { message: "Deleted Chapter succeed" };
   } catch (e) {
      console.log("Delete Chapter failed!", e);
   }
};

export const getLatestChaptersService = async () => {
   try {
      const latestChapters = await db.Chapter.findAll({
         order: [["createdAt", "DESC"]],
         limit: 10,
         attributes: ["title", "chapterNumber", "createdAt", "bookId"],
         include: [
            {
               model: db.Book,
               attributes: ["name"],
               include: [
                  {
                     model: db.User,
                     as: "Converter",
                     attributes: ["name"],
                  },
                  {
                     model: db.BookInfo,
                     include: [
                        {
                           model: db.Allcode,
                           as: "Author",
                           attributes: ["value"],
                        },
                        {
                           model: db.Allcode,
                           as: "Genre",
                           attributes: ["value"],
                        },
                     ],
                  },
               ],
            },
         ],
         raw: true,
         nest: true,
      });

      return latestChapters;
   } catch (e) {
      console.log(e);
   }
};

export const countWeeklyChaptersService = async (bookId) => {
   try {
      const chapters = await db.Chapter.findAll({
         where: {
            bookId: bookId,
            createdAt: {
               [Op.gte]: Sequelize.literal("NOW() - INTERVAL '7 DAY'"),
            },
         },
      });

      return chapters.length;
   } catch (e) {
      console.log(e);
   }
};
