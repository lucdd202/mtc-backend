import { Op, Sequelize } from "sequelize";
import db from "../models";
import { updateAnalyticService } from "./analyticService";
import { createNotificationService } from "./notificationService";

export const addNewCommentService = async (data) => {
   try {
      const history = await db.History.findOne({
         where: {
            userId: data.userId,
            bookId: data.bookId,
         },
      });

      const comment = await db.Comment.create({
         userId: data.userId,
         bookId: data.bookId,
         content: data.content,
         replyToId: 0, //cmt trên cùng
         commentAt: history ? history.readAt : null,
         isNotification: true,
      });

      if (comment) {
         updateAnalyticService(data.bookId, "TOTAL", "COMMENT");
         updateAnalyticService(data.bookId, "WEEK", "COMMENT");
         updateAnalyticService(data.bookId, "MONTH", "COMMENT");
      }

      return comment;
   } catch (e) {
      console.log(e);
   }
};

export const replyCommentService = async (data) => {
   try {
      const childComment = await db.Comment.create({
         userId: data.userId,
         content: data.content,
         replyToId: data.replyToId,
         isNotification: true,
      });

      const userReply = await db.User.findOne({ where: { id: data.userId } });
      const parentComment = await db.Comment.findOne({
         where: { id: data.replyToId },
      });
      const replies = await db.Comment.findAll({
         attributes: [
            [Sequelize.fn("DISTINCT", Sequelize.col("userId")), "userId"],
         ],
         where: {
            replyToId: data.replyToId,
            userId: { [Op.not]: [data.userId, parentComment.userId] },
         },
      });

      if (data.userId !== parentComment.userId) {
         if (parentComment.isNotification) {
            await createNotificationService({
               userId: parentComment.userId,
               entityId: parentComment.id,
               content: `${userReply.name} vừa trả lời bình luận của bạn`,
               fromId: data.userId,
               type: "COMMENT",
            });
         }

         await Promise.all(
            replies.forEach(async (reply) => {
               if (reply.isNotification) {
                  await createNotificationService({
                     userId: reply.userId,
                     entityId: reply.id,
                     content: `${userReply.name} vừa trả lời bình luận bạn đang quan tâm`,
                     fromId: data.userId,
                     type: "COMMENT",
                  });
               }
            })
         );
      }

      if (childComment) {
         updateAnalyticService(data.bookId, "TOTAL", "COMMENT");
         updateAnalyticService(data.bookId, "WEEK", "COMMENT");
         updateAnalyticService(data.bookId, "MONTH", "COMMENT");
      }
      return childComment;
   } catch (e) {
      console.log(e);
   }
};

export const getAllCommentsService = async (bookId) => {
   try {
      const comments = await db.Comment.findAll({
         where: { bookId: bookId, replyToId: 0 },
         order: [["createdAt", "DESC"]],
         include: [
            {
               model: db.User,
               attributes: ["createdAt", "name", "avatar"],
            },
            {
               model: db.Comment,
            },
         ],
         nest: true,
      });

      return comments;
   } catch (e) {
      console.log(e);
   }
};

export const getCommentRepliesService = async (replyToId) => {
   try {
      const replies = await db.Comment.findAll({
         where: { replyToId: replyToId },
         order: [["createdAt", "ASC"]],
         include: [
            {
               model: db.User,
               attributes: ["createdAt", "name", "avatar"],
            },
         ],
      });
      return replies;
   } catch (e) {
      console.log(e);
   }
};

export const deleteCommentService = async (idInput) => {
   try {
      await db.Comment.destroy({
         where: { id: idInput },
      });
      await db.Comment.destroy({
         where: { replyToId: idInput },
      });
      return { message: "Comment deleted!" };
   } catch (e) {
      console.log(e);
   }
};
