import db from "../models";

const randomKey = (dataType) => {
   return dataType + Math.floor(Math.random() * 1000);
};

export const addNewCodeService = async (data) => {
   try {
      let keyInput = randomKey(data.type);
      let newValue = data.value.replace(/\s+/g, " ").trim();
      const isCodeExist = await db.Allcode.findOne({
         where: {
            type: data.type,
            value: newValue,
         },
      });
      if (!isCodeExist) {
         while (true) {
            const response = await db.Allcode.findOne({
               where: { key: keyInput },
            });

            if (!response) {
               await db.Allcode.create({
                  type: data.type,
                  key: keyInput,
                  value: data.value,
               });
               return { message: "Add Code succeed !" };
            }

            // Nếu mã đã tồn tại, hãy tạo mã mới và kiểm tra lại
            keyInput = randomKey(data.type);
         }
      } else {
         return { message: "Tác giả đã tồn tại" };
      }
   } catch (e) {
      console.log("Add Code failed !", e);
   }
};

export const getAllCodesService = async (type) => {
   try {
      let codes = [];
      if (type === "ALL") {
         codes = await db.Allcode.findAll({
            raw: true,
         });
      } else {
         codes = await db.Allcode.findAll({
            where: { type: type },
            raw: true,
         });
      }
      console.log("Fetch all codes succeed!");
      if (codes) {
         return codes;
      } else {
         return { message: "No data" };
      }
   } catch (e) {
      console.log("Fetch all codes failed!", e);
   }
};

export const convertCodeService = async (keyInput) => {
   try {
      const code = await db.Allcode.findOne({
         where: { key: keyInput },
      });
      return code;
   } catch (e) {
      console.log("Convert Code failed!", e);
   }
};

export const deleteCodeService = async (inputId) => {
   try {
      await db.Allcode.destroy({
         where: { id: inputId },
      });
      return { message: "Deleted Code succeed" };
   } catch (e) {
      console.log("Delete Code failed!", e);
   }
};
