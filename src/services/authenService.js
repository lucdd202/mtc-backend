import db from "../models";
import bcrypt from "bcrypt";

const salt = bcrypt.genSaltSync(10);

export const loginService = async (data) => {
   try {
      const user = await db.User.findOne({
         where: {
            email: data.email,
         },
         raw: true,
      });
      if (user) {
         let check = bcrypt.compareSync(data.password, user.password);
         if (check) {
            return user.id;
         } else {
            return { message: "Wrong password!" };
         }
      } else {
         return { message: "User is not found!" };
      }
   } catch (e) {
      console.log("Login failed!", e);
   }
};

export const registerService = async (data) => {
   try {
      let hashPassword = bcrypt.hashSync(data.password, salt);
      let randomName = bcrypt.hashSync(data.email, salt);
      await db.User.create({
         email: data.email,
         password: hashPassword,
         roleId: "ROLE2",
         name: randomName,
         flower: 2,
         flowerGive: 0,
         chapterRead: 0,
      });
      return { message: "Register succeed !" };
   } catch (e) {
      console.log("Register failed !", e);
   }
};
