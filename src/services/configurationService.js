import db from "../models";

export const updateConfigurationService = async (data) => {
   try {
      const [configuration, created] = await db.Configuration.findOrCreate({
         where: {
            userId: data.userId,
         },
         defaults: {
            fontSize: data.fontSize,
            lineSpacing: data.lineSpacing,
            width: data.width,
            theme: data.theme,
            fontFamily: data.fontFamily,
         },
      });
      if (created) {
         return configuration;
      } else {
         configuration.fontSize = data.fontSize;
         configuration.lineSpacing = data.lineSpacing;
         configuration.width = data.width;
         configuration.theme = data.theme;
         configuration.fontFamily = data.fontFamily;
         await configuration.save();
         return configuration;
      }
   } catch (e) {
      console.log(e);
   }
};

export const getConfigurationService = async (userId) => {
   try {
      const configuration = await db.Configuration.findOne({
         where: {
            userId,
         },
      });
      return configuration;
   } catch (e) {
      console.log(e);
   }
};
