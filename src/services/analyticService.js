import db from "../models";

export const updateAnalyticService = async (bookId, type, key) => {
   try {
      const [view, created] = await db.Analytic.findOrCreate({
         where: { bookId: bookId, type: type, key: key },
         defaults: { count: 0 },
      });
      if (!created) {
         view.count += 1;
         await view.save();
      }
   } catch (e) {
      console.log(e);
   }
};

export const resetAnalyticService = async (type) => {
   try {
      //Type WEEK or MONTH
      const typeData = await db.Analytic.findAll({
         where: { type: type },
      });

      const resetPromises = typeData.map(async (data) => {
         data.count = 0;
         await data.save();
      });

      await Promise.all(resetPromises);
   } catch (e) {
      console.log(e);
   }
};

export const getAnalyticDataService = async (data) => {
   try {
      const response = await db.Analytic.findAll({
         where: {
            type: data.type,
            key: data.key,
         },
         include: [
            {
               model: db.Book,
               attributes: ["name", "image"],
            },
            {
               model: db.BookInfo,
               include: [
                  {
                     model: db.Allcode,
                     as: "Author",
                  },
                  {
                     model: db.Allcode,
                     as: "Genre",
                  },
               ],
            },
         ],
         order: [["count", "DESC"]],
         limit: 10,
         nest: true,
      });
      return response;
   } catch (e) {
      console.log(e);
   }
};
