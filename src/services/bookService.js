import { Op, Sequelize } from "sequelize";
import db from "../models";

export const addNewBookService = async (data) => {
   try {
      const saveBook = await db.Book.create({
         name: data.name,
         image: data.image,
         description: data.description,
         converterId: data.converterId,
      });
      if (saveBook) {
         await db.BookInfo.create({
            bookId: saveBook.id,
            authorId: data.authorId,
            genreId: data.genreId,
            statusId: data.statusId,
            povId: data.povId,
            worldId: data.worldId,
            featurId: data.featurId,
            characterId: data.characterId,
         });
      }
      return { message: "Add new book succeed !" };
   } catch (e) {
      console.log("Add new book failed !", e);
   }
};

export const getAllBooksService = async () => {
   try {
      const books = await db.Book.findAll({
         include: [
            {
               model: db.BookInfo,
               include: [
                  {
                     model: db.Allcode,
                     as: "Author",
                     attributes: ["value"],
                  },
                  {
                     model: db.Allcode,
                     as: "Genre",
                     attributes: ["value"],
                  },
                  {
                     model: db.Allcode,
                     attributes: ["value"],
                     as: "Character",
                  },
                  {
                     model: db.Allcode,
                     attributes: ["value"],
                     as: "POV",
                  },
                  {
                     model: db.Allcode,
                     attributes: ["value"],
                     as: "world",
                  },
                  {
                     model: db.Allcode,
                     attributes: ["value"],
                     as: "Feature",
                  },
                  {
                     model: db.Allcode,
                     attributes: ["value"],
                     as: "Status",
                  },
               ],
            },
         ],
         nest: true,
         raw: true,
      });
      //thích thì thêm limit
      return books;
   } catch (e) {
      console.log("Fetch all books failed!", e);
   }
};

export const getCompletedBooksService = async () => {
   try {
      const code = await db.Allcode.findOne({ where: { value: "Hoàn thành" } });
      const bookInfo = await db.BookInfo.findAll({
         where: { statusId: code.key },
      });
      const books = await db.Book.findAll({
         where: { id: bookInfo.map((item) => item.bookId) },
         include: [
            {
               model: db.BookInfo,
               order: [["updatedAt", "DESC"]],
               include: [
                  {
                     model: db.Allcode,
                     as: "Author",
                     attributes: ["value"],
                  },
                  {
                     model: db.Allcode,
                     as: "Genre",
                     attributes: ["value"],
                  },
                  {
                     model: db.Allcode,
                     attributes: ["value"],
                     as: "Status",
                  },
               ],
            },
         ],
         limit: 6,
         nest: true,
         raw: true,
      });

      return books;
   } catch (e) {
      console.log("Fetch all books failed!", e);
   }
};

export const getHighestRatedBooksService = async () => {
   try {
      const highestRatedBooks = await db.Book.findAll({
         subQuery: false,
         include: [
            {
               model: db.Rating,
               as: "Ratings",
               attributes: [],
               required: false,
            },
            {
               model: db.BookInfo,
               attributes: [],
               include: [
                  {
                     model: db.Allcode,
                     as: "Author",
                     attributes: ["value"],
                  },
                  {
                     model: db.Allcode,
                     as: "Genre",
                     attributes: ["value"],
                  },
               ],
            },
         ],
         attributes: {
            include: [
               [Sequelize.fn("AVG", Sequelize.col("Ratings.point")), "average"],
            ],
         },
         group: [
            "Book.id",
            "BookInfo.id",
            "BookInfo->Author.id",
            "BookInfo->Genre.id",
         ],
         order: [[Sequelize.literal("average"), "DESC"]],
         limit: 6,
         offset: 0,
         raw: true,
         nest: true,
      });

      return highestRatedBooks;
   } catch (e) {
      console.log(e);
   }
};

export const getConvertedBooksService = async (idInput) => {
   try {
      const books = await db.Book.findAll({
         where: { converterId: idInput },
         include: [
            {
               model: db.BookInfo,
               include: [
                  {
                     model: db.Allcode,
                     as: "Author",
                     attributes: ["value"],
                  },
                  {
                     model: db.Allcode,
                     as: "Genre",
                     attributes: ["value"],
                  },
                  {
                     model: db.Allcode,
                     attributes: ["value"],
                     as: "Character",
                  },
                  {
                     model: db.Allcode,
                     attributes: ["value"],
                     as: "POV",
                  },
                  {
                     model: db.Allcode,
                     attributes: ["value"],
                     as: "world",
                  },
                  {
                     model: db.Allcode,
                     attributes: ["value"],
                     as: "Feature",
                  },
                  {
                     model: db.Allcode,
                     attributes: ["value"],
                     as: "Status",
                  },
               ],
            },
         ],
         nest: true,
         raw: true,
      });
      //thích thì thêm limit
      return books;
   } catch (e) {
      console.log("Fetch all books failed!", e);
   }
};

export const getBookByIdService = async (data) => {
   try {
      const book = await db.Book.findOne({
         where: data,
         include: [
            {
               model: db.BookInfo,
               include: [
                  {
                     model: db.Allcode,
                     as: "Author",
                     attributes: ["value"],
                  },
                  {
                     model: db.Allcode,
                     as: "Genre",
                     attributes: ["value"],
                  },
                  {
                     model: db.Allcode,
                     attributes: ["value"],
                     as: "Character",
                  },
                  {
                     model: db.Allcode,
                     attributes: ["value"],
                     as: "POV",
                  },
                  {
                     model: db.Allcode,
                     attributes: ["value"],
                     as: "world",
                  },
                  {
                     model: db.Allcode,
                     attributes: ["value"],
                     as: "Feature",
                  },
                  {
                     model: db.Allcode,
                     attributes: ["value"],
                     as: "Status",
                  },
               ],
            },
            {
               model: db.Chapter,
            },
            {
               model: db.Bookmark,
            },
            {
               model: db.Analytic,
            },
            {
               model: db.Comment,
            },
            {
               model: db.Rating,
            },
            {
               model: db.User,
               as: "Converter",
               attributes: ["name"],
            },
         ],
         raw: false,
         nest: true,
      });
      console.log("Book found!");
      return book;
   } catch (e) {
      console.log("Book not found!", e);
   }
};

export const updateBookService = async (data) => {
   try {
      const book = await db.Book.update(
         {
            name: data.name,
            image: data.image,
            description: data.description,
            //do Converter update nên không cần converterId
         },
         {
            where: { id: data.id },
         }
      );

      const bookInfo = await db.BookInfo.update(
         {
            authorId: data.authorId,
            genreId: data.genreId,
            statusId: data.statusId,
            povId: data.povId,
            worldId: data.worldId,
            featurId: data.featurId,
            characterId: data.characterId,
         },
         {
            where: { bookId: data.id },
         }
      );

      if (!book || !bookInfo) {
         console.log("Missing parameter");
      }

      return { message: "Update Book succeed" };
   } catch (e) {
      console.log("Update Book failed!", e);
   }
};

export const deleteBookService = async (idInput) => {
   try {
      const book = await db.Book.destroy({
         where: { id: idInput },
      });
      if (book) {
         await db.BookInfo.destroy({
            where: { bookId: idInput },
         });
         await db.Analytic.destroy({
            where: { bookId: idInput },
         });
         await db.Banner.destroy({
            where: { bookId: idInput },
         });
         await db.Chapter.destroy({
            where: { bookId: idInput },
         });
         await db.Bookmark.destroy({
            where: { bookId: idInput },
         });
         // await db.Rating.destroy({
         //    where: { bookId: idInput },
         // });
         // await db.Comment.destroy({
         //    where: { bookId: idInput },
         // });
         // await db.Report.destroy({
         //    where: { entityId: idInput,type:"BOOK" },
         // });
      }

      console.log("Book deleted!");
      return { message: "Deleted Book succeed" };
   } catch (e) {
      console.log("Book deleted failed!", e);
   }
};

export const getNewestBooksService = async () => {
   try {
      const books = await db.Book.findAll({
         limit: 20,
         order: [["createdAt", "DESC"]],
         include: [
            {
               model: db.BookInfo,
               include: [
                  {
                     model: db.Allcode,
                     as: "Author",
                     attributes: ["value"],
                  },
                  {
                     model: db.Allcode,
                     as: "Genre",
                     attributes: ["value"],
                  },
               ],
            },
         ],
      });
      return books;
   } catch (e) {
      console.log(e);
   }
};

export const searchBooksService = async (data) => {
   try {
      const { text, options, sortBy } = data;
      let result = [];
      const whereConditions = [];

      for (const item of options) {
         let whereCondition = whereConditions.find(
            (condition) => condition.type === item.type
         );

         if (!whereCondition) {
            whereCondition = {
               type: item.type,
               [`${item.type.toLowerCase()}Id`]: [],
            };
            whereConditions.push(whereCondition);
         }

         whereCondition[`${item.type.toLowerCase()}Id`].push(item.key);
      }

      const allCombinations = whereConditions.reduce(
         (acc, { type, [`${type.toLowerCase()}Id`]: values }) => {
            if (acc.length === 0) {
               return values.map((value) => ({
                  [`${type.toLowerCase()}Id`]: value,
               }));
            }

            return acc.flatMap((combination) =>
               values.map((value) => ({
                  ...combination,
                  [`${type.toLowerCase()}Id`]: value,
               }))
            );
         },
         []
      );

      result = await db.Book.findAll({
         where: text !== null ? { name: { [Op.iLike]: `%${text}%` } } : {},
         include: [
            {
               model: db.BookInfo,
               where:
                  whereConditions.length > 0
                     ? {
                          [Op.or]: allCombinations,
                       }
                     : {},
               include: [
                  {
                     model: db.Allcode,
                     as: "Author",
                  },
                  {
                     model: db.Allcode,
                     as: "Genre",
                  },
               ],
            },
            {
               model: db.Bookmark,
               attributes: [],
            },
            {
               model: db.Comment,
               attributes: [],
            },
            {
               model: db.Chapter,
               attributes: [],
            },
            {
               model: db.Rating,
               attributes: [],
            },
            sortBy.key === "VIEW" || sortBy.key === "FLOWER"
               ? {
                    model: db.Analytic,
                    where: { type: sortBy.type, key: sortBy.key },
                 }
               : null,
         ].filter(Boolean),
         attributes:
            sortBy.key === "FLOWER" || sortBy.key === "VIEW"
               ? {}
               : {
                    include: [
                       [
                          Sequelize.fn(
                             "COUNT",
                             Sequelize.col("Bookmarks.bookId")
                          ),
                          "bookmarks_count",
                       ],
                       [
                          Sequelize.fn(
                             "COUNT",
                             Sequelize.col("Comments.bookId")
                          ),
                          "comments_count",
                       ],
                       [
                          Sequelize.fn(
                             "COUNT",
                             Sequelize.col("Chapters.bookId")
                          ),
                          "chapters_count",
                       ],
                       [
                          Sequelize.fn("AVG", Sequelize.col("Ratings.point")),
                          "average",
                       ],
                    ],
                 },
         order:
            sortBy.key === "CREATED"
               ? [["createdAt", "DESC"]]
               : sortBy.key === "UPDATED"
               ? [["updatedAt", "DESC"]]
               : sortBy.key === "VIEW" || sortBy.key === "FLOWER"
               ? [[db.Analytic, "count", "DESC"]]
               : sortBy.key === "BOOKMARK"
               ? [[Sequelize.literal("bookmarks_count"), "DESC"]]
               : sortBy.key === "COMMENT"
               ? [[Sequelize.literal("comments_count"), "DESC"]]
               : sortBy.key === "CHAPTER"
               ? [[Sequelize.literal("chapters_count"), "DESC"]]
               : sortBy.key === "RATING" && sortBy.key === "COUNT"
               ? [[Sequelize.literal("chapters_count"), "DESC"]]
               : sortBy.key === "RATING" && sortBy.key === "POINT"
               ? [[Sequelize.literal("average"), "DESC"]]
               : [],

         group:
            sortBy.key === "VIEW" || sortBy.key === "FLOWER"
               ? [
                    "Book.id",
                    "BookInfo.id",
                    "BookInfo->Author.id",
                    "BookInfo->Genre.id",
                    "Analytics.id",
                 ]
               : [
                    "Book.id",
                    "BookInfo.id",
                    "BookInfo->Author.id",
                    "BookInfo->Genre.id",
                 ],
         raw: true,
         nest: true,
      });

      result = await result.filter((item) => item.BookInfo);

      return result;
   } catch (e) {
      console.log(e);
   }
};

export const getBooksByRankService = async (data) => {
   try {
      const books = await db.Book.findAll({
         include: [
            {
               model: db.Analytic,
               where: { type: data.rankType, key: data.rankKey },
            },
            {
               model: db.BookInfo,
               include: [
                  {
                     model: db.Allcode,
                     as: "Author",
                     attributes: ["value"],
                  },
                  {
                     model: db.Allcode,
                     as: "Genre",
                     attributes: ["value"],
                  },
               ],
            },
         ],
         order: [[db.Analytic, "count", "DESC"]],
         raw: true,
         nest: true,
      });

      return books;
   } catch (e) {
      console.log(e);
   }
};
