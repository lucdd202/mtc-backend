import db from "../models";

export const createNotificationService = async (data) => {
   try {
      const notification = await db.Notification.create({
         userId: data.userId,
         entityId: data.entityId,
         type: data.type,
         content: data.content,
         fromId: data.fromId,
         isRead: false,
      });

      return notification;
   } catch (e) {
      console.log(e);
   }
};

const notificationTypeToModel = {
   CHAPTER: db.Chapter,
   COMMENT: db.Comment,
};

export const getAllNotificationsService = async (idInput) => {
   try {
      const notifications = await db.Notification.findAll({
         where: { userId: idInput },
         include: [
            {
               model: db.User,
               attributes: ["avatar"],
            },
         ],
         nest: true,
         raw: true,
      });

      await Promise.all(
         notifications.map(async (notification) => {
            const model = notificationTypeToModel[notification.type];
            if (model) {
               const result = await model.findOne({
                  where: { id: notification.entityId },
               });
               if (result) {
                  notification.bookId = result.bookId;
               }
            }
         })
      );

      return notifications;
   } catch (e) {
      console.log(e);
   }
};

export const deleteNotificationService = async (idInput) => {
   try {
      const notification = await db.Notification.destroy({
         where: { id: idInput },
      });
      return notification;
   } catch (e) {
      console.log(e);
   }
};

export const checkNotificationService = async (data) => {
   try {
      const condition = data.id ? { id: data.id } : { userId: data.userId };
      const result = await db.Notification.update(
         {
            isRead: true,
         },
         {
            where: condition,
         }
      );
      return result;
   } catch (e) {
      console.log(e);
   }
};

export const rejectNotificationService = async (idInput) => {
   try {
      const notification = await db.Notification.findOne({
         where: { id: idInput },
      });

      const model = notificationTypeToModel[notification.type];
      const result = await model.findOne({
         where: { id: notification.entityId },
      });

      if (notification.type === "COMMENT") {
         result.isNotification = false;
         await result.save();
      }
      if (notification.type === "CHAPTER") {
         const history = await db.History.findOne({
            where: { userId: notification.userId, bookId: result.bookId },
         });
         history.isNotification = false;
         await history.save();
      }
      return { mesage: "succeed!" };
   } catch (e) {
      console.log(e);
   }
};
