import db from "../models";

export const addNewBannerService = async (data) => {
   try {
      const banner = await db.Banner.create({
         bookId: data.bookId,
         image: data.image,
      });
      return banner;
   } catch (e) {
      console.log(e);
   }
};

export const deleteBannerService = async (idInput) => {
   try {
      const banner = await db.Banner.destroy({
         where: {
            id: idInput,
         },
      });
      return banner;
   } catch (e) {
      console.log(e);
   }
};

export const getBannerByIdService = async (idInput) => {
   try {
      const banner = await db.Banner.findOne({
         where: {
            id: idInput,
         },
      });
      return banner;
   } catch (e) {
      console.log(e);
   }
};

export const getAllBannersService = async () => {
   try {
      const banners = await db.Banner.findAll();
      return banners;
   } catch (e) {
      console.log(e);
   }
};

export const updateBannerService = async (data) => {
   try {
      const banner = await db.Banner.update(
         {
            image: data.image,
         },
         {
            where: {
               id: data.id,
            },
         }
      );
      return banner;
   } catch (e) {
      console.log(e);
   }
};

const getRandomBanners = (array, numElements) => {
   if (numElements <= 0 || numElements > array.length) {
      console.log("Số lượng đối tượng ngẫu nhiên không hợp lệ");
   }

   const randomBanners = [];
   const copyArray = [...array];

   for (let i = 0; i < numElements; i++) {
      const randomIndex = Math.floor(Math.random() * copyArray.length);
      randomBanners.push(copyArray.splice(randomIndex, 1)[0]);
   }
   if (randomBanners.length > 1) {
      return randomBanners;
   } else {
      return randomBanners[0];
   }
};

export const getRandomBannerService = async (number) => {
   try {
      const banners = await db.Banner.findAll();
      const random = getRandomBanners(banners, number);
      return random;
   } catch (e) {
      console.log(e);
   }
};
