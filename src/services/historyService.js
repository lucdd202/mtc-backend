import db from "../models";

export const updateCreateHistoryService = async (data) => {
   try {
      const [history, created] = await db.History.findOrCreate({
         where: {
            bookId: data.bookId,
            userId: data.userId,
         },
         defaults: {
            readAt: 1,
            isNotification: false,
         },
      });

      if (!created && history) {
         history.readAt = parseInt(data.chapterNumber);
         await history.save();
      }

      return history;
   } catch (e) {
      console.log("Add or Update history failed !", e);
   }
};

export const getHistoryByIdService = async (data) => {
   try {
      const history = await db.History.findOne({
         where: {
            userId: data.userId,
            bookId: data.bookId,
         },
         raw: true,
      });

      return history;
   } catch (e) {
      console.log(e);
   }
};

export const getAllHistoriesService = async (data) => {
   try {
      let histories = {};

      histories = await db.History.findAll({
         where: { userId: data.userId },
         include: [
            {
               model: db.Book,
               include: [
                  {
                     model: db.Chapter,
                  },
                  {
                     model: db.BookInfo,
                     include: [{ model: db.Allcode, as: "Author" }],
                  },
               ],
            },
         ],
         order: [["updatedAt", "DESC"]],
         limit: data.limit ?? null,
         nest: true,
      });

      return histories;
   } catch (e) {
      console.log("Fetch all historys failed!");
      console.log(e);
   }
};

export const deleteHistoryService = async (idInput) => {
   try {
      await db.History.destroy({
         where: { id: idInput },
      });

      return { message: "Deleted History succeed" };
   } catch (e) {
      console.log(e);
   }
};

export const switchNotificationService = async (idInput) => {
   try {
      const history = await db.History.findOne({
         where: { id: idInput },
      });
      if (history) {
         history.isNotification = !history.isNotification;
         await history.save();
      }

      return { message: "Turn on/off notification succeed!" };
   } catch (e) {
      console.log(e);
   }
};
