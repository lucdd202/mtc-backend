import db from "../models";

export const createNewRatingService = async (data) => {
   try {
      const {
         userId,
         bookId,
         characterPoint,
         storyPoint,
         worldPoint,
         content,
      } = data;
      const averagePoint = (+characterPoint + +storyPoint + +worldPoint) / 3;
      const [rating, created] = await db.Rating.findOrCreate({
         where: {
            userId,
            bookId,
         },
         defaults: {
            characterPoint,
            storyPoint,
            worldPoint,
            content,
            point: averagePoint.toFixed(2),
         },
      });

      if (created) {
         return { code: 0, message: "Thêm đánh giá thành công !" };
      } else {
         return { code: 1, message: "Bạn đã đánh giá truyện này rồi!" };
      }
   } catch (e) {
      console.log(e);
   }
};

export const getNewestRatingsService = async (limit) => {
   try {
      const ratings = await db.Rating.findAll({
         limit: limit,
         include: [
            {
               model: db.User,
            },
            {
               model: db.Book,
            },
         ],
         order: [["createdAt", "DESC"]],
      });
      return ratings;
   } catch (e) {
      console.log(e);
   }
};

export const getAllRatingService = async (bookId) => {
   try {
      const ratings = await db.Rating.findAll({
         where: { bookId },
         include: [
            {
               model: db.User,
            },
         ],
      });
      return ratings;
   } catch (e) {
      console.log(e);
   }
};

export const deleteRatingService = async (ratingId) => {
   try {
      const ratings = await db.Rating.destroy({
         where: { id: ratingId },
      });
      if (ratings) {
         await db.Rating.destroy({
            where: { replyToId: ratingId },
         });
      }
      return { message: "Rating deleted!" };
   } catch (e) {
      console.log(e);
   }
};

export const replyRatingService = async (data) => {
   try {
      const reply = await db.Rating.create({
         userId: data.userId,
         content: data.content,
         replyToId: data.replyToId,
      });
      return reply;
   } catch (e) {
      console.log(e);
   }
};

export const getRatingRepliesService = async (replyToId) => {
   try {
      const replies = await db.Rating.findAll({
         where: { replyToId: replyToId },
         include: [
            {
               model: db.User,
               attributes: ["createdAt", "name", "avatar"],
            },
         ],
      });
      return replies;
   } catch (e) {
      console.log(e);
   }
};
